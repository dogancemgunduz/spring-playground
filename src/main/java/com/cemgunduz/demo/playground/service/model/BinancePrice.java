package com.cemgunduz.demo.playground.service.model;

import lombok.Data;

@Data
public class BinancePrice {
  private String symbol;
  private Double price;
}
