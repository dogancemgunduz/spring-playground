package com.cemgunduz.demo.playground.service;

import com.cemgunduz.demo.playground.repository.CryptoPriceRepository;
import com.cemgunduz.demo.playground.repository.model.CryptoPriceSnapshot;
import com.cemgunduz.demo.playground.service.model.BinancePrice;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CryptoPriceService {

  private final RestTemplate restTemplate = new RestTemplate();

  private final CryptoPriceRepository cryptoPriceRepository;

  public CryptoPriceService(CryptoPriceRepository cryptoPriceRepository) {
    this.cryptoPriceRepository = cryptoPriceRepository;
  }

  @Value("${binance.url}")
  private String binanceURL;

  //@Value("${USERID}")
  @Value("${binance.token:MYTOKEN}")
  private String binanceToken;

  private Map<String, Double> priceMap = new HashMap<>();
  private Set<String> registry = new HashSet<>();

  public Double getPriceByName(String name){

    if(!priceMap.containsKey(name)){
      registerPrice(name);
    }

    return priceMap.get(name);
  }

  public List<CryptoPriceSnapshot> getAllRecordedPricesSince(String name, Date date){

    return cryptoPriceRepository.findByNameAndDateGreaterThan(name, date);
  }

  @Scheduled(fixedRate = 10000)
  public void updateRegistry(){
    System.out.println("Scheduled job working with token " + binanceToken);
    registry.stream().forEach(
        crypto -> registerPrice(crypto)
    );
  }

  private void registerPrice(String name){

    Double price = restTemplate.getForEntity(binanceURL.replace("{name}", name.toUpperCase()), BinancePrice.class)
        .getBody().getPrice();

    registry.add(name);
    priceMap.put(name, price);

    cryptoPriceRepository.save(
        CryptoPriceSnapshot.builder()
            .id(UUID.randomUUID().toString())
            .name(name)
            .price(price)
            .date(new Date())
            .build()
    );
  }
}
