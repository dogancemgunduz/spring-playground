package com.cemgunduz.demo.playground.service;

import lombok.SneakyThrows;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class VeryLengthyCalculationService {

  // lame implementation
  @SneakyThrows
  @Cacheable(value = "myCache", key = "#myInt")
  public long calculate(int myInt, String myStr){

    Thread.sleep(2000);

    return Long.valueOf(myInt);
  }
}
