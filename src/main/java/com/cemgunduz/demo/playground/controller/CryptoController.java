package com.cemgunduz.demo.playground.controller;

import com.cemgunduz.demo.playground.controller.model.CryptoPrice;
import com.cemgunduz.demo.playground.service.CryptoPriceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CryptoController {

  private CryptoPriceService cryptoPriceService;

  public CryptoController(CryptoPriceService cryptoPriceService) {
    this.cryptoPriceService = cryptoPriceService;
  }

  @GetMapping("/crypto/{name}")
  private CryptoPrice getByName(@PathVariable String name) {
    return CryptoPrice.builder()
        .name(name)
        .price(cryptoPriceService.getPriceByName(name))
        .build();
  }
}
