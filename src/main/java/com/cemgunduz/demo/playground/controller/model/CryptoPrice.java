package com.cemgunduz.demo.playground.controller.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CryptoPrice {
  public String name;
  public double price;
}
