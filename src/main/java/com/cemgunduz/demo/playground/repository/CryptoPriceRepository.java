package com.cemgunduz.demo.playground.repository;

import com.cemgunduz.demo.playground.repository.model.CryptoPriceSnapshot;
import java.util.Date;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CryptoPriceRepository extends MongoRepository<CryptoPriceSnapshot, String> {

  List<CryptoPriceSnapshot> findByNameAndDateGreaterThan(String name, Date date);
}
