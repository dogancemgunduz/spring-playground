package com.cemgunduz.demo.playground.repository.model;

import java.util.Date;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
@Builder
public class CryptoPriceSnapshot {

  @Id
  private final String id;
  private final String name;
  private final Double price;
  private final Date date;
}
