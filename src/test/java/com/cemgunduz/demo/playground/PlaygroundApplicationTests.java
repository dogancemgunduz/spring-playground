package com.cemgunduz.demo.playground;

import com.cemgunduz.demo.playground.service.CryptoPriceService;
import com.cemgunduz.demo.playground.service.VeryLengthyCalculationService;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
class PlaygroundApplicationTests {

	@Autowired
	VeryLengthyCalculationService service;

	@Autowired
	CryptoPriceService cryptoPriceService;

	@Test
	void contextLoads() {

		IntStream.range(1,10).forEach(
				ignored -> service.calculate(5, UUID.randomUUID().toString())
		);
	}

	@Test
	void myDatabaseTest() {

		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);

		Assert.isTrue(
				cryptoPriceService.getAllRecordedPricesSince("eth", cal.getTime()).size() > 0, "contains an entry"
		);
	}

}
