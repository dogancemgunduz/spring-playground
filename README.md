# Spring Playground

This is an example project I would like to use to showcase some of my favourite features of spring. I would like to grow it over time to add some new features and keep it as simple as possible. This project is for educational use only.

Requires a default installation of mongo in order to work correctly as part of showcasing spring mongo integration.

## Installation
```bash
mvn clean install
```
